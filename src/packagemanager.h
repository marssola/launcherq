#ifndef APPLICATION_H
#define APPLICATION_H

#include <QObject>
#include <QAbstractListModel>
#ifdef Q_OS_ANDROID
#include <QAndroidJniObject>
#endif

class Application : public QObject
{
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY applicationChanged)
    Q_PROPERTY(QString package READ package WRITE setPackage NOTIFY applicationChanged)
    Q_OBJECT
public:
    explicit Application(QObject *parent = nullptr) : QObject(parent) {}
    Application(const QString &app_name, const QString &package_name, QObject *parent = nullptr) :
        QObject(parent),
        m_name(app_name),
        m_package(package_name)
    {
        emit applicationChanged();
    }

    inline QString name() { return m_name; }
    inline void setName(QString app_name) { m_name = app_name; emit applicationChanged(); }
    inline QString package() { return m_package; }
    inline void setPackage(QString package_name) { m_package = package_name; emit applicationChanged(); }

signals:
    void applicationChanged();

private:
    QString m_name;
    QString m_package;
};

class PackageManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariant applications READ getApplications NOTIFY applicationsChanged)
public:
    explicit PackageManager(QObject *parent = nullptr);

    static QString jstringToQString(jstring string);
    Q_INVOKABLE void registerBroadcast();
    Q_INVOKABLE void launchApplication(const QString &package);
    Q_INVOKABLE inline void emitAddApplication(const QString &name, const QString &packageName) {emit newApplicationDetected(name, packageName);}
    Q_INVOKABLE inline void emitRemoveApplication(const QString &packageName) {emit removedApllication(packageName);}
    inline QVariant getApplications() { return QVariant::fromValue(m_applications); }

signals:
    void newApplicationDetected(const QString &name, const QString &packageName);
    void removedApllication(const QString &packageName);
    void applicationsChanged();

public slots:
    void retrievePackages();

private:
    QList<QObject *> m_applications;
    QAndroidJniObject m_android_activity;
    QAndroidJniObject m_intent_filter;
    QAndroidJniObject m_broadcast_receiver;

   void registerNativeMethods();
};

#endif // APPLICATION_H
