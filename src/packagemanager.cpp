#include "packagemanager.h"
#include <QImage>
#include <QtAndroidExtras>
#include <QAndroidJniObject>
#include <QAndroidJniEnvironment>

static void packageAdded(JNIEnv *env, jobject thiz, jstring label, jstring packageName, jlong qtObject)
{
    Q_UNUSED(env)
    Q_UNUSED(thiz)
    reinterpret_cast<PackageManager*>(qtObject)->emitAddApplication(PackageManager::jstringToQString(label).simplified(), PackageManager::jstringToQString(packageName));
}

static void packageRemoved(JNIEnv *env, jobject thiz, jstring packageName, jlong qtObject)
{
    Q_UNUSED(env)
    Q_UNUSED(thiz)
    reinterpret_cast<PackageManager*>(qtObject)->emitRemoveApplication(PackageManager::jstringToQString(packageName));
}

PackageManager::PackageManager(QObject *parent) :
    QObject(parent),
    m_android_activity(QAndroidJniObject::callStaticObjectMethod("org/qtproject/qt5/android/QtNative", "activity", "()Landroid/app/Activity;")),
    m_intent_filter(QAndroidJniObject("android/content/IntentFilter")),
    m_broadcast_receiver(QAndroidJniObject("com/marssola/launcherq/PackageChanged"))
{
}

QString PackageManager::jstringToQString(jstring string)
{
    QAndroidJniEnvironment env;
    jboolean jfalse = false;
    return QString(env->GetStringUTFChars(string, &jfalse));
}

void PackageManager::retrievePackages()
{
    qDebug() << ">>>>>>>>>>>>>>" << Q_FUNC_INFO << "Retrieve Packages";
    QAndroidJniObject connectionType = QAndroidJniObject::callStaticObjectMethod("com/marssola/launcherq/LauncherQ",
                                                                           "applications",
                                                                           "()[Lcom/marssola/launcherq/Application;");
    jobjectArray array = connectionType.object<jobjectArray>();

    QAndroidJniEnvironment env;
    jsize size = env->GetArrayLength(array);
    for (int i = 0; i < size; ++i) {
        QAndroidJniObject obj = env->GetObjectArrayElement(array, i);
        QString name = jstringToQString((obj.callObjectMethod<jstring>("getName")).object<jstring>());
        QString packagneName = jstringToQString((obj.callObjectMethod<jstring>("getPackageName")).object<jstring>());

        qDebug() << "Application: " << name << "package name: " << packagneName;
        m_applications.append(new Application(name, packagneName));
        emit applicationsChanged();
    }
}

void PackageManager::registerBroadcast()
{
    retrievePackages();
    qDebug() << ">>>>>>>>>>>>" << Q_FUNC_INFO << "Registering Broadcast";
    QAndroidJniObject add_action_string = QAndroidJniObject::fromString("android.intent.action.PACKAGE_ADDED");
    QAndroidJniObject remove_action_string = QAndroidJniObject::fromString("android.intent.action.PACKAGE_REMOVED");
    QAndroidJniObject data_scheme_string = QAndroidJniObject::fromString("package");

    m_intent_filter.callMethod<void>("addAction", "(Ljava/lang/String;)V", add_action_string.object<jstring>());
    m_intent_filter.callMethod<void>("addAction", "(Ljava/lang/String;)V", remove_action_string.object<jstring>());
    m_intent_filter.callMethod<void>("addDataScheme", "(Ljava/lang/String;)V", data_scheme_string.object<jstring>());
    m_android_activity.callObjectMethod("registerReceiver",
                                        "(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;",
                                        m_broadcast_receiver.object<jobject>(), m_intent_filter.object<jobject>());
    registerNativeMethods();
}

void PackageManager::registerNativeMethods()
{
    qDebug() << ">>>>>>>> Register Native Methods";
    JNINativeMethod methods[] {{"jpackageRemoved", "(Ljava/lang/String;J)V", reinterpret_cast<void *>(packageRemoved)},
        {"jpackageAdded", "(Ljava/lang/String;Ljava/lang/String;J)V", reinterpret_cast<void *>(packageAdded)}};

    QAndroidJniObject::callStaticMethod<void>("com/marssola/launcherq/PackageChanged",
                                              "setQtObject", "(J)V",
                                              "(J)V", reinterpret_cast<long>(this));

    QAndroidJniEnvironment env;
    jclass objectClass = env->GetObjectClass(m_broadcast_receiver.object<jobject>());
    if (env->ExceptionCheck())
        env->ExceptionClear();
    env->RegisterNatives(objectClass, methods, sizeof(methods) / sizeof(methods[0]));
    if (env->ExceptionCheck())
        env->ExceptionClear();
    env->DeleteLocalRef(objectClass);
    if (env->ExceptionCheck())
        env->ExceptionClear();
}

void PackageManager::launchApplication(const QString &package)
{
    QAndroidJniObject::callStaticMethod<void>("com/marssola/launcherq/LauncherQ",
                                              "launchApplication",
                                              "(Ljava/lang/String;)V",
                                              QAndroidJniObject::fromString(package).object<jstring>());
}
