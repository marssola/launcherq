#include "androidimageprovider.h"
#include <QImage>
#ifdef Q_OS_ANDROID
#include <QtAndroidExtras>
#include <QAndroidJniObject>
#include <QAndroidJniEnvironment>
#endif
#include <QDebug>

AndroidImageProvider::AndroidImageProvider() :
    QQuickImageProvider(QQuickImageProvider::Image)
{
}

QImage AndroidImageProvider::requestImage(const QString &id, QSize *size, const QSize &requestedSize)
{
    Q_UNUSED(size)
    Q_UNUSED(requestedSize)

    QImage img;
    QAndroidJniObject image_object;
    qDebug() << "Image ID: " << id;
    if (id == "wallpaper") {
        image_object = QAndroidJniObject::callStaticObjectMethod("com/marssola/launcherq/LauncherQ",
                                                                                   "getWallpaper",
                                                                                   "()[B");
    } else {
        image_object = QAndroidJniObject::callStaticObjectMethod("com/marssola/launcherq/LauncherQ",
                                                                                   "getApplicationIcon",
                                                                                   "(Ljava/lang/String;)[B",
                                                                                   QAndroidJniObject::fromString(id).object<jstring>());
    }

    QAndroidJniEnvironment env;
    jbyteArray imageDataArray = image_object.object<jbyteArray>();
    if (!imageDataArray)
        return img;

    jsize imageSize = env->GetArrayLength(imageDataArray);
    if (imageSize > 0) {
        jboolean jfalse = false;
        jbyte *image = env->GetByteArrayElements(imageDataArray, &jfalse);
        img = QImage::fromData(reinterpret_cast<uchar*>(image), imageSize, "PNG");
        env->ReleaseByteArrayElements(imageDataArray, image, JNI_ABORT);
    }
    return img;
}

