import QtQuick 2.12
import QtQuick.Controls 2.5
import QtGraphicalEffects 1.0

Item {
    id: root
    property alias text: label.text
    property string packageName
    property alias mouseArea: mousearea

    Column {
        anchors.fill: parent

        Item {
            width: parent.width
            height: settings.icon_size

            Image {
                id: image

                anchors.centerIn: parent
                width: settings.icon_size * 0.8
                height: settings.icon_size * 0.8

                source: "image://android/" + root.packageName
                fillMode: Image.PreserveAspectFit
                asynchronous: true

                layer.enabled: true
                layer.effect: OpacityMask {
                    maskSource: Rectangle {
                        anchors.fill: parent
                        x: image.x
                        y: image.y

                        width: image.width
                        height: image.height
                        radius: 15
                    }
                }
            }
        }


        Label {
            id: label

            width: parent.width
            height: 24

            elide: Text.ElideRight
            horizontalAlignment: Label.AlignHCenter
            wrapMode: Label.WordWrap
            font.pixelSize: 10
            color: "#fff"

            layer.enabled: true
            layer.effect: DropShadow {
                horizontalOffset: 1
                verticalOffset: 1
                samples: 5
                color: "#000"
                spread: 0
            }
        }
    }

    MouseArea {
        id: mousearea
        anchors.fill: parent
    }
}
