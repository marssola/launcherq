import QtQuick 2.12
import QtQuick.Controls 2.5
import com.marssola.LauncherQ 1.0 as LQ

import "../../components"

ScrollView {
//    property alias model: appGrid.model

    width: window.width
    height: window.height

    Grid {
        id: appGrid
        width: parent.width
        columns: parseInt(window.width / settings.icon_size)
        padding: (window.width % settings.icon_size) / 2

        Repeater {
            model: LQ.PackageManager.applications

            ApplicationSimple {
                width: settings.icon_size
                height: settings.icon_size + 24

                text: modelData.name
                packageName: modelData.package

                mouseArea.onClicked: {
                    console.log("Launch application:", modelData.package)
                    LQ.PackageManager.launchApplication(modelData.package)
                }
            }
        }
    }

    /*
    GridView {
        id: appGrid
        anchors.fill: parent

        model: LQ.PackageManager.applications

        delegate: ApplicationSimple {
            width: 80
            height: 100

            text: modelData.name
        }
    }
    */
}
