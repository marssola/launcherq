import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Window 2.2
import QtGraphicalEffects 1.0
import Qt.labs.settings 1.1
import com.marssola.LauncherQ 1.0 as LQ

ApplicationWindow {
    id: window
    visible: true
    title: "LauncherQ"

    Settings {
        id: settings

        property string theme: "simple"
        property int icon_size: 70
    }

    property var resolutions: [
        {"height": 480, "width": 320}, // HVGA
        {"height": 640, "width": 480}, // VGA
        {"height": 800, "width": 480}, // WVGA
        {"height": 800, "width": 600}, // SVGA
        {"height": 640, "width": 360}, // nHD
        {"height": 960, "width": 540}  // qHD
    ]
    property int currentResolution: 3
    width: resolutions[currentResolution].width
    height: resolutions[currentResolution].height

    Image {
        id: wallpaper
        anchors.fill: parent
        source: "image://android/wallpaper"
        fillMode: Image.PreserveAspectCrop
    }

    Timer {
        interval: 550
        running: true

        onTriggered: {
            console.log("Trigger")
            LQ.PackageManager.registerBroadcast()
        }
    }

    Loader {
        id: loaderTheme
        anchors.fill: parent
        source: "qrc:/ui/themes/" + settings.theme + "/ApplicationGrid.qml"
    }

    Component.onCompleted: console.log(window.width, window.height)
}
