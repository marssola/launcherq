package com.marssola.launcherq

import android.app.ActivityManager
import android.app.WallpaperManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.AdaptiveIconDrawable
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import java.io.ByteArrayOutputStream
import java.util.*

open class LauncherQ : org.qtproject.qt5.android.bindings.QtActivity() {
    init {
        instance = this
    }
    
    override fun onCreate(savedInstanceState : Bundle?) {
        this.QT_ANDROID_DEFAULT_THEME = "AppTheme"
        
        super.onCreate(savedInstanceState)
        getWindow().getDecorView()
        .setSystemUiVisibility(
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION)
    }
    
    override fun onStart() {
        super.onStart()
        instance = this
        
        val activityManager = instance!!.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        mPm = instance!!.getPackageManager() as PackageManager
    }
    
    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        jnewIntent(qtObject)
    }
    
    companion object {
        private val TAG = "LauncherQ"
        var instance: LauncherQ ? = null
            private set
        private var mPm: PackageManager ? = null
        private var qtObject: Long = 0    
            
        @JvmStatic
        fun launchApplication(packageName: String) {
            val intent = mPm!!.getLaunchIntentForPackage(packageName)

            if (intent != null) {
                instance!!.startActivity(intent)
            }
        }
        
        @JvmStatic
        fun applications(): Array<Application> {
            val applications = mutableListOf<Application>()
            try {
                val intent = Intent(Intent.ACTION_MAIN, null)
                intent.addCategory(Intent.CATEGORY_LAUNCHER)
                val availableActivities = mPm!!.queryIntentActivities(intent, 0)

                for (i in availableActivities.indices) {
                    applications.add(Application(availableActivities[i].loadLabel(mPm).toString(), availableActivities[i].activityInfo.packageName))
                }

                for (i in applications.indices) {
                    if (applications[i].name.isNotEmpty() && (Character.isSpaceChar(applications[i].name[0]) || Character.isWhitespace(applications[i].name[0]))) {
                        val charToReplace = applications[i].name[0]
                        applications[i].name = applications[i].name.replace(charToReplace, ' ').trim { it <= ' ' }
                        applications[i].name = applications[i].name.trim { it <= ' ' }
                    }
                }
                
                applications.sort()
            } catch (e: Exception) {
                Log.e(TAG, "applications", e)
            }
            return applications.toTypedArray()
        }
        
        fun isAppLaunchable(packageName : String) : Boolean {
            return mPm!!.getLaunchIntentForPackage(packageName) != null
        }
        
        fun getApplicationLabel(packageName : String) : String {
            try {
                val app = mPm!!.getApplicationInfo(packageName, 0)
                val resources = mPm!!.getResourcesForApplication(app)
                val resolveInfo = mPm!!.resolveActivity(mPm!!.getLaunchIntentForPackage(packageName), 0)
                return resolveInfo.loadLabel(mPm).toString()
            } catch (e : Exception) {
                Log.e(TAG, "getApplicationLabel for $packageName", e)
                return ""
            }
        }
            
        fun setQtObject(qtObject : Long) {
            LauncherQ.qtObject = qtObject
        }

        @JvmStatic
        fun getWallpaper() : ByteArray {
//         fun getWallpaper() : String {
            val stream = ByteArrayOutputStream()
            var bitmap : Bitmap? = null
            val wm: WallpaperManager? = WallpaperManager.getInstance(instance) as WallpaperManager
            var wallpaperDrawable : Drawable? = null
            try {
                wallpaperDrawable = wm?.getDrawable()
                bitmap = (wallpaperDrawable as BitmapDrawable).bitmap
                bitmap?.compress(Bitmap.CompressFormat.PNG, 100, stream)
            } catch (e: Exception) {
                Log.e(TAG, "error: ", e)
            }
            return stream.toByteArray()
        }
        
        @JvmStatic
        fun getApplicationIcon(packageName : String) : ByteArray {
            val stream = ByteArrayOutputStream()
            var bitmap : Bitmap? = null
            var icon : Drawable? = null
            try {
                icon = mPm!!.getApplicationIcon(packageName)
                if (icon is AdaptiveIconDrawable) {
                    val arrayOfDrawables = arrayOf(icon.background, icon.foreground)
                    val layerDrawable = LayerDrawable(arrayOfDrawables)
                    
                    bitmap = Bitmap.createBitmap(layerDrawable.intrinsicWidth, layerDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
                    
                    val canvas = Canvas(bitmap)
                    layerDrawable.setBounds(0, 0, canvas.width, canvas.height)
                    layerDrawable.draw(canvas)
                } else {
                    bitmap = (icon as BitmapDrawable).bitmap
                }
                bitmap?.compress(Bitmap.CompressFormat.PNG, 100, stream)
                return stream.toByteArray()
            } catch (e: Exception) {
                Log.e(TAG, "error: ", e)
            }

            return stream.toByteArray()
        }
        
        val defaultApplicationIcon: Drawable
            get() = instance!!.getResources().getDrawable(android.R.mipmap.sym_def_app_icon)
        
        @JvmStatic
        fun callStringMethod() : String {
            val str = "String on Kotlin"
            return str
        }
        
        @JvmStatic
        fun callIntMethod() : Int {
            return 100
        }
        
        @JvmStatic
        private external fun jnewIntent(qtObject: Long)
    }
}
