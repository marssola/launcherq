#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQml/QQmlContext>

#include "src/packagemanager.h"
#include "src/androidimageprovider.h"

static QObject *package_manager(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)
    return new PackageManager();
}

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QCoreApplication::setOrganizationName("Marssola");
    QCoreApplication::setOrganizationDomain("marssola.com");
    QCoreApplication::setApplicationName("LauncherQ");

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

//    engine.rootContext()->setContextProperty("PackageManager", new PackageManager);
    qmlRegisterSingletonType<PackageManager>("com.marssola.LauncherQ", 1, 0, "PackageManager", package_manager);
    engine.addImageProvider(QLatin1String("android"), new AndroidImageProvider());

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
